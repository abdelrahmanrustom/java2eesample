package com.ibm.recommendedmovies.service;

import com.ibm.recommendedmovies.entities.entity.Genre;
import com.ibm.recommendedmovies.entities.entity.Movie;
import com.ibm.recommendedmovies.entities.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class RecommendedMoviesStoreService {

    private static final Logger LOG = LoggerFactory.getLogger(RecommendedMoviesStoreService.class);
    @PersistenceContext(name = "movies", unitName = "movies")
    EntityManager em;


    public boolean createPerson(Person person) {
        LOG.info("RecommendedMoviesStoreService: creating Person - {}", person.toString());
        em.persist(person);
        return true;
    }

    public boolean createGenre(Genre genre) {
        LOG.info("RecommendedMoviesStoreService: creating Person - {}", genre.toString());
        em.persist(genre);
        return true;
    }
    public boolean createMovie(Movie movie) {
        LOG.info("RecommendedMoviesStoreService: creating Movie - {}",  movie.toString());
        em.persist(movie);
        return true;
    }

}
