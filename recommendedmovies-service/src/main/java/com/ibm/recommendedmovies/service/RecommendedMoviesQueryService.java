package com.ibm.recommendedmovies.service;

import com.ibm.recommendedmovies.entities.entity.Genre;
import com.ibm.recommendedmovies.entities.entity.Movie;
import com.ibm.recommendedmovies.entities.entity.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class RecommendedMoviesQueryService {

    private static final Logger LOG = LoggerFactory.getLogger(RecommendedMoviesQueryService.class);
    @PersistenceContext(name = "movies", unitName = "movies")
    EntityManager em;

    public List<Person> findPersons() {
        LOG.info("RecommendedMoviesQueryService: retrieving Persons - {}");
        try {
            Query query = em.createNamedQuery("find persons");
            List<Person> result = (List<Person>) query.getResultList();
//            em.merge(result);
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }


    public List<Genre> findGenres() {
        LOG.info("RecommendedMoviesQueryService: retrieving Genres - {}");
        try {
            Query query = em.createNamedQuery("find genres");
            List<Genre> result = (List<Genre>) query.getResultList();
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Movie> findMovieByYear(int year) {
        LOG.info("RecommendedMoviesQueryService: retrieving Movies by year - {}");
        try {
            Query query = em.createNamedQuery("find movie by year");
            query.setParameter("year", year);
            List<Movie> result = (List<Movie>) query.getResultList();
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Movie> findMovieByProducerName(String producerName) {
        LOG.info("RecommendedMoviesQueryService: retrieving Movies by Producer Name - {}");
        try {
            Query query = em.createNamedQuery("find movie by producerName");
            query.setParameter("producerName", producerName);
            List<Movie> result = (List<Movie>) query.getResultList();
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Movie> findMovieByActorName(String actorName) {
        LOG.info("RecommendedMoviesQueryService: retrieving Movies by Actor Name - {}");
        try {
            Query query = em.createNamedQuery("find movie by actorName");
            query.setParameter("actorName", actorName);
            List<Movie> result = (List<Movie>) query.getResultList();
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }


    public List<Movie> findMovieByGenreName(String genreName) {
        LOG.info("RecommendedMoviesQueryService: retrieving Movies by Actor Name - {}");
        try {
            Query query = em.createNamedQuery("find movie by genreName");
            query.setParameter("genreName", genreName);
            List<Movie> result = (List<Movie>) query.getResultList();
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }

    public Object deleteMovieByName(String movieName) {
        LOG.info("RecommendedMoviesQueryService: deleting Movies by Movie Name - {}");
        try {
            Query query = em.createQuery(
                    "delete Movie m where m.movieName = :movieName");
            int deletedCount = query.setParameter("movieName", movieName).executeUpdate();

            return deletedCount;
        } catch (NoResultException e) {
            return null;
        }
    }

}