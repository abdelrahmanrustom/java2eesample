package com.ibm.recommendedmovies.entities.entity;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person")
@Access(AccessType.FIELD)
@NamedQuery(query = "select p from Person p", name = "find persons")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "personName")
    private String personName;

    @Column(name = "personJob")
    private String personJob;

    @ManyToMany(mappedBy = "actors")
    private List<Movie> movies = new ArrayList<Movie>();

    public Person(String personName, String personJob) {
        this.personName = personName;
        this.personJob = personJob;
    }
    public Person(){

    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonJob() {
        return personJob;
    }

    public void setPersonJob(String personJob) {
        this.personJob = personJob;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", personName='" + personName + '\'' +
                ", personJob='" + personJob +
                '}';
    }
}