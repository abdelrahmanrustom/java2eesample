package com.ibm.recommendedmovies.entities.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "movie")
@Access(AccessType.FIELD)

@NamedQueries(value =
        {
                @NamedQuery(query = "select m from Movie m where m.movieYear = :year", name = "find movie by year"),
                @NamedQuery(query = "select m from Movie m where m.producer.personName = :producerName and m.producer.personJob = 'Producer'", name = "find movie by producerName"),
                @NamedQuery(query = "select m,p from Movie m join m.actors p where p.personName = :actorName and p.personJob = 'Actor'", name = "find movie by actorName"),
                @NamedQuery(query = "select m,g from Movie m join m.genres g where g.genreName = :genreName", name = "find movie by genreName"),

        })
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "movieName")
    private String movieName;

    @Column(name = "movieYear")
    private Integer movieYear;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "producer_fk")
    private Person producer;

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinTable(name = "movie_actor",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id")
    )
    private Set<Person> actors;

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinTable(name = "movie_genre",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id")
    )
    private Set<Genre> genres;

    public Movie() {
    }

    public Movie(BigInteger id, String movieName, Integer movieYear, Person producer, Set<Person> actors, Set<Genre> genres) {
        this.id = id;
        this.movieName = movieName;
        this.movieYear = movieYear;
        this.producer = producer;
        this.actors = actors;
        this.genres = genres;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Integer getMovieYear() {
        return movieYear;
    }

    public void setMovieYear(Integer movieYear) {
        this.movieYear = movieYear;
    }

    public Person getProducer() {
        return producer;
    }

    public void setProducer(Person producer) {
        this.producer = producer;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Set<Person> getActors() {
        return actors;
    }

    public void setActors(Set<Person> actors) {
        this.actors = actors;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }

//    public void addActors(Person actor) {
//        this.actors.add(actor);
//    }
//    public void addGen


    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", movieName='" + movieName +
                ", movieYear='" + movieYear  +
                ", producer='" + producer+
                '}';
    }

}
