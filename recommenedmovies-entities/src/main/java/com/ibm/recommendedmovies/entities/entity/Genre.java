package com.ibm.recommendedmovies.entities.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "genre")
@Access(AccessType.FIELD)
//@NamedQuery(query = "select a from AuditTrail a where a.appId = :pid", name = "find audit by pid")
@NamedQuery(query = "select g from Genre g", name = "find genres")
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private BigInteger id;
    @Column(name = "genreName")
    private String genreName;

    @XmlTransient()
    @ManyToMany(mappedBy = "genres")
    private Set<Movie> movies;

    public Genre(String genreName) {
        this.genreName = genreName;
    }

    public Genre() {

    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    @XmlTransient()
    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", genreName='" + genreName +
                '}';
    }
}