package com.ibm.recommendedmovies.rest;

import com.ibm.recommendedmovies.entities.entity.Genre;
import com.ibm.recommendedmovies.entities.entity.Movie;
import com.ibm.recommendedmovies.entities.entity.Person;
import com.ibm.recommendedmovies.service.RecommendedMoviesQueryService;
import com.ibm.recommendedmovies.service.RecommendedMoviesStoreService;
import jdk.nashorn.internal.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Path("/api")
@RequestScoped
public class RecommendedMoviesRest {

    private static final Logger LOG = LoggerFactory.getLogger(RecommendedMoviesRest.class);

   // private List<String> endpointsList = new ArrayList<>();

    @EJB
    private RecommendedMoviesStoreService storeService;

    @EJB
    private RecommendedMoviesQueryService queryService;

    @GET
    @Path("getPeople")
    @Produces("application/json")
    public Response getPeople() {
        return Response.ok(queryService.findPersons()).build();
    }

    @GET
    @Path("getGenres")
    @Produces("application/json")
    public Response getGenres() {
        return Response.ok(queryService.findGenres()).build();
    }

    @GET
    @Path("getMovies/year/{year}")
    @Produces("application/json")
    public Response getMoviesByYear(@PathParam("year") int year) {
        return Response.ok(queryService.findMovieByYear(year)).build();
    }

    @GET
    @Path("getMovies/producer/{producerName}")
    @Produces("application/json")
    public Response getMoviesByProducer(@PathParam("producerName") String producerName) {
        return Response.ok(queryService.findMovieByProducerName(producerName)).build();
    }

    @GET
    @Path("getMovies/actor/{actorName}")
    @Produces("application/json")
    public Response getMoviesByActor(@PathParam("actorName") String actorName) {
        return Response.ok(queryService.findMovieByActorName(actorName)).build();
    }

    @GET
    @Path("getMovies/genre/{genreName}")
    @Produces("application/json")
    public Response getMoviesByGenre(@PathParam("genreName") String genreName) {
        return Response.ok(queryService.findMovieByGenreName(genreName)).build();
    }


    @DELETE
    @Path("deleteMovie/{movieName}")
    @Produces("application/json")
    public Response DeleteMovieByName(@PathParam("movieName") String movieName) {
        return Response.ok(queryService.deleteMovieByName(movieName)).build();
    }


    @POST
    @Path("addPerson")
    @Consumes("application/json")
    public Response createPerson(JSONParser person) {
        JSONObject item;
        LOG.info("RecommendedMoviesStoreService: creating Person - {}", person.toString());
        return storeService.createPerson(person) ? Response.ok(true).build()
                : Response.ok(false).build();
    }


    @POST
    @Path("addGenre/genre/{genreName}")
    @Consumes("application/json")
    public Response createGenre(@PathParam("genreName") String genreName) {
        Genre genre = new Genre(genreName);
        LOG.info("RecommendedMoviesStoreService: creating Genre - {}", genre.toString());
        return storeService.createGenre(genre) ? Response.ok(true).build()
                : Response.ok(false).build();
    }



    @POST
    @Path("addMovie")
    @Consumes("application/json")
    public Response createMovie(Movie movie) {
        LOG.info("RecommendedMoviesStoreService: creating Movie - {}", movie.toString());
        return storeService.createMovie(movie) ? Response.ok(true).build()
                : Response.ok(false).build();
    }

}